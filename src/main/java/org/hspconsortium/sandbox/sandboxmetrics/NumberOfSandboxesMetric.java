//package org.hspconsortium.sandbox.sandboxmetrics;
//
//import com.amazonaws.services.cloudwatch.model.Dimension;
//import com.amazonaws.services.cloudwatch.model.MetricDatum;
//import com.amazonaws.services.cloudwatch.model.StandardUnit;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//
//@Service
//public class NumberOfSandboxesMetric {
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    private CloudWatchMetricPublisher cloudWatchMetricPublisher;
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    public NumberOfSandboxesMetric(CloudWatchMetricPublisher cloudWatchMetricPublisher) {
//        this.cloudWatchMetricPublisher = cloudWatchMetricPublisher;
//    }
//
//    public MetricDatum getNumberOfSandboxesMetric(int sandboxType) {
//        int sandboxCount = jdbcTemplate.queryForObject("SELECT count(*) FROM sandbox WHERE api_endpoint_index=" + sandboxType, Integer.class);
//
//        Date now = new Date();
//
//        Dimension dimension = new Dimension()
//                .withName("DBInstanceIdentifier")
//                .withValue("hspc-sandbox-db");
//
//        MetricDatum datum = new MetricDatum()
//                .withUnit(StandardUnit.Count)
//                .withValue((double) sandboxCount)
//                .withDimensions(dimension)
//                .withTimestamp(now);
//
//        return datum;
//    }
//
//    @Scheduled(fixedRateString = "${hspc.platform.sandbox.metrics.frequent_rate_ms}")
//    public void getFrequentMetrics() {
//        MetricDatum datum = getNumberOfSandboxesMetric(1);
//        datum.setMetricName("SANDBOXES_LOGINS");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//    }
//
//    @Scheduled(fixedRateString = "${hspc.platform.sandbox.metrics.frequent_rate_ms}")
//    public void getInfrequentMetrics() {
//        logger.info("getInfrequentMetrics()");
//
//        // FHIR DSTU2 V1
//        MetricDatum datum = getNumberOfSandboxesMetric(1);
//        datum.setMetricName("SANDBOXES_DSTU2_V1");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//
//        // FHIR STU3
//        datum = getNumberOfSandboxesMetric(2);
//        datum.setMetricName("SANDBOXES_STU3_1.8_V2");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//
//        datum = getNumberOfSandboxesMetric(3);
//        datum.setMetricName("SANDBOXES_STU3_1.9_V3");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//
//        datum = getNumberOfSandboxesMetric(4);
//        datum.setMetricName("SANDBOXES_STU3_V4");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//
//        datum = getNumberOfSandboxesMetric(5);
//        datum.setMetricName("SANDBOXES_DSTU2_V5");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//
//        datum = getNumberOfSandboxesMetric(6);
//        datum.setMetricName("SANDBOXES_STU3_V6");
//        cloudWatchMetricPublisher.publish("SANDBOX/sandbox-instances", datum);
//    }
//}
